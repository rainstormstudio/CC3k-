#include "game.hpp"
#include "./core/managers/configsManager.hpp"
#include "./core/managers/assetsManager.hpp"
#include "./core/managers/windowManager.hpp"
#include "./core/managers/keyboardManager.hpp"
#include "./core/managers/gameStateManager.hpp"
#include "./gameStates/titleState.hpp"
#include "./utilities/utilities.hpp"
#include <iostream>

bool Game::init() {
    if (!CM.init("./configs/gameSettings.cfg")) {
        return false;
    }
    if (!AM.init()) {
        return false;
    }
    if (!WM.init()) {
        return false;
    }
    if (!KBM.init()) {
        return false;
    }

    return true;
}

void Game::run() {
    GSM.addScreen(std::make_shared<TitleState>());

    _looping = true;
    while (WM.isWindowOpen() && _looping) {
        float deltaTime = WM.getDeltaTime();

        KBM.clear();
        while (WM.pollEvent(KBM.getEvent())) {
            if (!KBM.update()) {
                _looping = false;
                // WM.closeWindow();
            }
        }

        if (!GSM.update(deltaTime)) {
            _looping = false;
        }
    }

    WM.closeWindow();
}
