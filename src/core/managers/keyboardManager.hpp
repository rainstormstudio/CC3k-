/**
 * @file keyboardManager.hpp
 * @author Hongyu Ding (rainstormstudio@yahoo.com)
 * @brief This file defines a keyboard manager
 * @version 0.1
 * @date 2021-05-22
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef KEYBOARDMANAGER_HPP
#define KEYBOARDMANAGER_HPP

#include <SFML/Window/Event.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <array>

#define KBM KeyboardManager::getInstance()

class KeyboardManager {
private:
    KeyboardManager() {}

    sf::Event _event;
    std::array<bool, sf::Keyboard::Key::KeyCount> _pressed;
    std::array<bool, sf::Keyboard::Key::KeyCount> _released;
public:
    KeyboardManager(KeyboardManager const&) = delete;
    void operator=(KeyboardManager const&) = delete;

    static KeyboardManager& getInstance() {
        static KeyboardManager instance;
        return instance;
    }

    sf::Event& getEvent() {
        return _event;
    }

    bool init();
    void clear();
    bool update();

    bool keyPressed(sf::Keyboard::Key key);
    bool keyReleased(sf::Keyboard::Key key);
};

#endif
