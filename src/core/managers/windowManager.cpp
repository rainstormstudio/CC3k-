#include "windowManager.hpp"
#include "configsManager.hpp"
#include "assetsManager.hpp"
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/Transform.hpp>
#include <SFML/Window/VideoMode.hpp>
#include <iostream>

#include "../types.hpp"

bool WindowManager::isWindowOpen() {
    return _window->isOpen();
}

bool WindowManager::pollEvent(sf::Event& event) {
    return _window->pollEvent(event);
}

float WindowManager::getDeltaTime() {
    sf::Time time = _clock.getElapsedTime();
    _clock.restart();
    return time.asSeconds();
}

void WindowManager::setView(sf::View view) {
    _window->setView(view);
}

sf::View WindowManager::defaultView() const {
    return _window->getDefaultView();
}

bool WindowManager::init() {
    if (!sf::Shader::isAvailable()) {
        std::cerr << "Shaders are not available in this machine." << std::endl;
        return false;
    }
    _window = std::make_unique<sf::RenderWindow>(
        sf::VideoMode(
            ConfigsManager::getInstance().screenWidth(), 
            ConfigsManager::getInstance().screenHeight()),
        "CC3k+",
        sf::Style::Titlebar | sf::Style::Close
    );
    //_window->setFramerateLimit(60);
    _window->setVerticalSyncEnabled(ConfigsManager::getInstance().verticalSync());
    _window->setFramerateLimit(ConfigsManager::getInstance().frameRateLimit());
    _window->setPosition(Vec2i(
                             (sf::VideoMode::getDesktopMode().width - ConfigsManager::getInstance().screenWidth()) / 2,
                             (sf::VideoMode::getDesktopMode().height - ConfigsManager::getInstance().screenHeight()) / 2));
    _window->setKeyRepeatEnabled(true);

    return true;
}

void WindowManager::clear() {
    _window->clear();
}

void WindowManager::render() {
    _window->display();
}

void WindowManager::draw(const sf::Texture& texture, float x, float y, float scale) {
    sf::Sprite sprite;
    sprite.setTexture(texture);
    sprite.setPosition(x, y);
    sprite.setScale(scale, scale);
    _window->draw(sprite);
}

void WindowManager::drawSprite(sf::Sprite& sprite, IntRect region, float x, float y, float scale) {
    sprite.setTextureRect(region);
    sprite.setPosition(x, y);
    sprite.setScale(scale, scale);
    _window->draw(sprite);
}

void WindowManager::drawOnVertexArray(const sf::VertexArray& vertexArray, const sf::Texture& texture, float x, float y, float scale) {
    sf::RenderStates states;
    sf::Transform transform; {
        transform.translate(x, y);
        transform.scale(scale, scale);
    }
    states.texture = &texture;
    states.transform = transform;
    _window->draw(vertexArray, states);
}

void WindowManager::sDraw(const sf::Texture& texture, const sf::Shader& shader, float x, float y, float scale) {
    sf::Sprite sprite;
    sprite.setTexture(texture);
    sprite.setPosition(x, y);
    sprite.setScale(scale, scale);
    _window->draw(sprite, &shader);
}

void WindowManager::write(std::string content, float x, float y, sf::Color color, unsigned int fontSize) {
    sf::Text text;
    text.setFont(AssetsManager::getInstance().mainFont());
    text.setString(content);
    text.setCharacterSize(fontSize);
    text.setPosition(x, y);
    text.setFillColor(color);
    _window->draw(text);
}

void WindowManager::writeF(std::string content, sf::Font& font, float x, float y, sf::Color color, unsigned int fontSize) {
    sf::Text text;
    text.setFont(font);
    text.setString(content);
    text.setCharacterSize(fontSize);
    text.setPosition(x, y);
    text.setFillColor(color);
    _window->draw(text);
}

void WindowManager::closeWindow() {
    _window->close();
}
