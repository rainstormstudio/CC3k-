#include "keyboardManager.hpp"

bool KeyboardManager::init() {
    clear();

    return true;
}

void KeyboardManager::clear() {
    _released.fill(false);
}

bool KeyboardManager::update() {
    if (_event.type == sf::Event::Closed) {
        return false;
    }

    if (_event.type == sf::Event::KeyPressed) {
        _pressed[_event.key.code] = true;
    }
    if (_event.type == sf::Event::KeyReleased) {
        _pressed[_event.key.code] = false;
        _released[_event.key.code] = true;
    }

    return true;
}

bool KeyboardManager::keyPressed(sf::Keyboard::Key key) {
    return _pressed[key];
}

bool KeyboardManager::keyReleased(sf::Keyboard::Key key) {
    return _released[key];
}
