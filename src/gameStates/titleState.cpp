#include "titleState.hpp"
#include "../core/managers/windowManager.hpp"
#include "../core/managers/configsManager.hpp"
#include "../core/managers/assetsManager.hpp"
#include "../core/managers/keyboardManager.hpp"
#include "../core/types.hpp"
#include "../gameStates/inGameState.hpp"

#include <iostream>

void TitleState::init() {

}

bool TitleState::update(float deltaTime) {
    if (KBM.keyReleased(sf::Keyboard::Return)) {
        GSM.addScreen(std::make_shared<InGameState>());
        return true;
    }

    static float t = 0.0f;
    t += deltaTime * 0.1f;
    if (t > 2 * 3.1415926f) {
        t = 0.0f;
    }
    AM.titleShader().setUniform("t", t);
    AM.titleShader().setUniform("screenWidth", CM.screenWidth());
    AM.titleShader().setUniform("screenHeight", CM.screenHeight());

    WM.clear();
    WM.sDraw(
        AM.whiteTexture(),
        AM.titleShader(),
        0.0f,
        0.0f,
        2.0f
    );
    WM.draw(
        AM.titleTexture(),
        0.0f,
        0.0f,
        2.0f
    );
    Vec2f infoPos(60.0f, 440.0f);
    sf::Color infoColor(200, 150, 70);
    WM.write("VERSION " + CM.version(), infoPos.x, infoPos.y, infoColor);
    WM.write("by " + CM.author(), infoPos.x, infoPos.y + 20.0f, infoColor);
    WM.write("(" + CM.authorEmail() + ")", infoPos.x, infoPos.y + 40.0f, infoColor);
    WM.write(CM.copyRight(), infoPos.x, infoPos.y + 60.0f, infoColor);

    sf::Color alertColor;
    static float alertTime = 0.0f;
    alertTime += deltaTime;
    if (alertTime > 1.0f) {
        alertTime = 0.0f;
    }
    if (alertTime >= 0.5f) {
        alertColor = sf::Color(100, 100, 100);
    } else {
        alertColor = sf::Color::White;
    }
    WM.write("PRESS ENTER", 344.0f, 340.0f, alertColor, 24);

    WM.render();

    return true;
}
