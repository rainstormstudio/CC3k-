/**
 * @file pauseState.hpp
 * @author Hongyu Ding (rainstormstudio@yahoo.com)
 * @brief This file defines the pause state
 * @version 0.1
 * @date 2022-04-22
 *
 * @copyright Copyright (c) 2022
 */
#ifndef PAUSESTATE_HPP
#define PAUSESTATE_HPP

#include "../core/managers/gameStateManager.hpp"

class PauseState : public GameState {
public:
    void init() override;
    bool update(float deltaTime) override;
};

#endif // PAUSESTATE_HPP
