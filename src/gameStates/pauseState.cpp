#include "pauseState.hpp"
#include "../core/managers/windowManager.hpp"
#include "../core/managers/keyboardManager.hpp"
#include <cstdio>

void PauseState::init() {

}

bool PauseState::update(float deltaTime) {
    if (KBM.keyReleased(sf::Keyboard::Escape)) {
        GSM.popScreen();
        return true;
    }

    WM.clear();

    WM.write("PAUSE SCREEN", 0, 0);
    WM.write("PRESS ESC TO CONTINUE", 0, 30);

    WM.render();

    return true;
}
