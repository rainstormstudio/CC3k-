#include "inGameState.hpp"
#include "../core/managers/windowManager.hpp"
#include "../core/managers/keyboardManager.hpp"
#include "../core/managers/configsManager.hpp"
#include "../core/ecs/ecs.hpp"

#include "../components/transform.hpp"
#include "../components/camera.hpp"
#include "../components/floor.hpp"
#include "pauseState.hpp"
#include <SFML/Window/Keyboard.hpp>
#include <cmath>
#include <cstdio>

void InGameState::init() {
    ECS.init();

    ECS.registerComponent<Transform>();
    ECS.registerComponent<Camera>();
    ECS.registerComponent<Floor>();

    _cameraUpdateSystem = ECS.addSystem<CameraUpdateSystem>(); {
        Signature signature;
        signature.set(ECS.getComponentType<Transform>());
        signature.set(ECS.getComponentType<Camera>());
        ECS.setSystemSignature<CameraUpdateSystem>(signature);
    }

    _floorUpdateSystem = ECS.addSystem<FloorUpdateSystem>(); {
        Signature signature;
        signature.set(ECS.getComponentType<Transform>());
        signature.set(ECS.getComponentType<Floor>());
        ECS.setSystemSignature<FloorUpdateSystem>(signature);
    }

    _floorRenderSystem = ECS.addSystem<FloorRenderSystem>(); {
        Signature signature;
        signature.set(ECS.getComponentType<Transform>());
        signature.set(ECS.getComponentType<Floor>());
        ECS.setSystemSignature<FloorRenderSystem>(signature);
    }

    Entity cameraEntity = ECS.addEntity(); {
        ECS.addComponent(
            cameraEntity,
            Transform{
                .position = Vec2f(
                    CM.screenWidth() / 2.0f,
                    CM.screenHeight() / 2.0f), // TODO: change the default camera position
                .rotation = 0.0f,
                .scale = 1.0f
            }
        );
        ECS.addComponent(cameraEntity,
                                        Camera{
                                            .view = sf::View(Vec2f(0.0f, 0.0f),
                                                             Vec2f(
                                                                 CM.screenWidth(),
                                                                 CM.screenHeight())),
                                            .moveSpeed = 200.0f});
    }
    Entity map = ECS.addEntity(); {
        ECS.addComponent(map, Floor{});
        ECS.addComponent(map, Transform{.position = Vec2f(0, 0), .rotation = 0.0f, .scale = 1.0f});
    }

    _cameraUpdateSystem->init();
    _floorUpdateSystem->init();
    _floorRenderSystem->init(cameraEntity);

}

bool InGameState::update(float deltaTime) {
    if (KBM.keyReleased(sf::Keyboard::Escape)) {
        GSM.addScreen(std::make_shared<PauseState>());
        return true;
    }

    // update systems
    _cameraUpdateSystem->update(deltaTime);
    _floorUpdateSystem->update(deltaTime);

    // renderable systems
    WM.clear();

    _floorRenderSystem->update(deltaTime);

    if (CM.showFPS()) {
        WM.write("FPS: " + std::to_string((int)std::round(1.0f / deltaTime)));
    }
    WM.render();

    return true;
}
