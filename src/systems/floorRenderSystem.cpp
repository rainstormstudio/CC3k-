#include "floorRenderSystem.hpp"

#include <string>
#include <iostream>

#include "../core/managers/windowManager.hpp"
#include "../core/managers/assetsManager.hpp"
#include "../core/managers/configsManager.hpp"
#include "../core/ecs/ecs.hpp"
#include "../components/transform.hpp"
#include "../components/camera.hpp"
#include "../components/floor.hpp"

void FloorRenderSystem::init(Entity cameraEntity) {
    _cameraEntity = cameraEntity;
}

void FloorRenderSystem::update(float deltaTime) {
    auto& camera = ECS.getComponent<Camera>(_cameraEntity);

    WM.setView(camera.view);

    for (auto& entity : _entities) {
        auto& transform = ECS.getComponent<Transform>(entity);
        auto& floor = ECS.getComponent<Floor>(entity);

        WM.drawOnVertexArray(
            floor.vertices, AM.floorTilesTexture());
    }

    WM.setView(
        WM.defaultView()
    );
}
