#include "floorUpdateSystem.hpp"

#include "../core/ecs/ecs.hpp"
#include "../components/floor.hpp"

void FloorUpdateSystem::init() {

}

void FloorUpdateSystem::update(float deltaTime) {
    for (auto& entity : _entities) {
        auto& floor = ECS.getComponent<Floor>(entity);
        if (!floor.active) {
            Floor::generate(floor, 80, 60);
        }
        for (int i = 0; i < floor.data.size(); i ++) {
            for (int j = 0; j < floor.data[i].size(); j ++) {
                int index = static_cast<int>(floor.data[i][j]);
                int x = (index % 16) * 32;
                int y = (index / 16) * 32;

                sf::Vertex* quad = &floor.vertices[(i * floor.data[i].size() + j) * 4];
#define OFFSET_A 0
#define OFFSET_B 32
                quad[0].texCoords = Vec2f(x + OFFSET_A, y + OFFSET_A) + Vec2f(0.5f, 0.5f);
                quad[1].texCoords = Vec2f(x + OFFSET_B, y + OFFSET_A) + Vec2f(-0.5f, 0.5f);
                quad[2].texCoords = Vec2f(x + OFFSET_B, y + OFFSET_B) + Vec2f(-0.5f, -0.5f);
                quad[3].texCoords = Vec2f(x + OFFSET_A, y + OFFSET_B) + Vec2f(0.5f, -0.5f);
            }
        }
    }
}
