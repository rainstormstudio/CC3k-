#include "cameraUpdateSystem.hpp"

#include "../core/ecs/ecs.hpp"
#include "../core/managers/keyboardManager.hpp"
#include "../core/managers/configsManager.hpp"
#include "../components/camera.hpp"
#include "../components/transform.hpp"
#include <cmath>

void CameraUpdateSystem::init() {

}

void CameraUpdateSystem::update(float deltaTime) {
    for (auto& entity : _entities) {
        auto& transform = ECS.getComponent<Transform>(entity);
        auto& camera = ECS.getComponent<Camera>(entity);
        if (KBM.keyPressed(sf::Keyboard::Left)) {
            transform.position.x -= camera.moveSpeed * deltaTime;
        }
        if (KBM.keyPressed(sf::Keyboard::Right)) {
            transform.position.x += camera.moveSpeed * deltaTime;
        }
        if (KBM.keyPressed(sf::Keyboard::Up)) {
            transform.position.y -= camera.moveSpeed * deltaTime;
        }
        if (KBM.keyPressed(sf::Keyboard::Down)) {
            transform.position.y += camera.moveSpeed * deltaTime;
        }

        camera.view.setCenter(std::round(transform.position.x), std::round(transform.position.y));
        camera.view.setRotation(transform.rotation);
    }
}
